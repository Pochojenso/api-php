<?php 

/**
 * Category Class
 */
class Category
{
	# Database stuff
	private $conn;
	private $table = 'categories';
	
	# Category properties
	public $id;
	public $category_name;
	public $created_at;

	# Constructor with Database
	public function __construct($db)
	{
		$this->conn = $db;
	}

	# Get All Categories (READ METHOD)
	public function getCategories() : object
	{
		$query = 'SELECT
					id,
					name,
					created_at
				FROM
					' . $this->table . '
				ORDER BY
					id DESC
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Execute Query
		$stmt->execute();

		return $stmt;
	}

	# Get Single Category (READ SINGLE METHOD)
	public function getCategory() : void
	{
		$query = 'SELECT
					id,
					name
				FROM
				' . $this->table .'
				WHERE
					id = :id LIMIT 0,1
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Bind ID
		$stmt->bindParam(':id', $this->id);

		# Execute Query
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row)
		{
			# SET Properties
			$this->id = $row['id'];
			$this->category_name = $row['name'];
		}
	}

	# Create Category (CREATE METHOD)
	public function createCategory() : bool
	{
		$query = 'INSERT INTO '
					. $this->table . '
				(
					name
				)
				VALUES
				(
					:name
				)';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Clean Data
		$this->name = htmlspecialchars(strip_tags($this->name));

		# Bind data
		$stmt->bindParam(':name', $this->name);

		# Execute Query
		$result = $stmt->execute();

		if ($result)
		{
			return true;
		}

		printf("ERROR: %s. \n", $stmt->error);

		return false;
	}

	# Update Category (UPDATE METHOD)
	public function updateCategory() : bool
	{
		$query = 'UPDATE '
					. $this->table . '
				SET
					name = :name
				WHERE
					id = :id
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Clean Data
		$this->name = htmlspecialchars(strip_tags($this->name));

		$this->id = htmlspecialchars(strip_tags($this->id));

		# Bind data
		$stmt->bindParam(':name', $this->name);

		$stmt->bindParam(':id', $this->id);

		# Execute Query
		$result = $stmt->execute();

		if ($result)
		{
			return true;
		}

		printf("ERROR: %s. \n", $stmt->error);

		return false;
	}

	# Delete Category (DELETE METHOD)
	public function deleteCategory() : bool
	{
		$query = 'DELETE FROM '
					. $this->table . '
				WHERE
					id = :id
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Clean Data
		$this->id = htmlspecialchars(strip_tags($this->id));

		# Bind ID
		$stmt->bindParam(':id', $this->id);

		# Execute Query
		$exec = $stmt->execute();

		if ($exec)
		{
			return true;
		}

		printf("ERROR: %s. \n", $stmt->error);

		return false;
	}
}




 ?>