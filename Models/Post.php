<?php

/**
 * Post Class
 */
class Post
{
	# Database stuff
	private $conn;
	private $table = 'posts';

	# Post properties
	public $id;
	public $category_id;
	public $category_name;
	public $title;
	public $body;
	public $author;
	public $created_at;

	# Constructor with Database
	public function __construct($db)
	{
		$this->conn = $db;
	}

	# Get All Posts (READ METHOD)
	public function getPosts() : object
	{
		$query = 'SELECT
					c.name AS category_name,
					p.id,
					p.category_id,
					p.title,
					p.body,
					p.author,
					p.created_at
				FROM
					' . $this->table . ' p
				LEFT JOIN 
					categories c ON p.category_id = c.id
				ORDER BY
					p.created_at DESC
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Execute Query
		$stmt->execute();

		return $stmt;
	}

	# Get Single Post (READ SINGLE METHOD)
	public function getPost() : void
	{
		$query = 'SELECT
					c.name AS category_name,
					p.id,
					p.category_id,
					p.title,
					p.body,
					p.author,
					p.created_at
				FROM
					' . $this->table . ' p
				LEFT JOIN 
					categories c ON p.category_id = c.id
				WHERE
					p.id = :id LIMIT 0,1
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Bind ID
		$stmt->bindParam(':id', $this->id);

		# Execute Query
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row)
		{
			# SET Properties
			$this->title = $row['title'];
			$this->body = $row['body'];
			$this->author = $row['author'];
			$this->category_id = $row['category_id'];
			$this->category_name = $row['category_name'];
		}
	}

	# Create Post (CREATE METHOD)
	public function createPost() : bool
	{
		$query = 'INSERT INTO '
					. $this->table . '
				(
					title,
					body,
					author,
					category_id
				)
				VALUES
				(
					:title,
					:body,
					:author,
					:category_id
				)';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Clean Data
		$this->title = htmlspecialchars(strip_tags($this->title));
		$this->body = htmlspecialchars(strip_tags($this->body));
		$this->author = htmlspecialchars(strip_tags($this->author));
		$this->category_id = htmlspecialchars(strip_tags($this->category_id));

		# Bind data
		$stmt->bindParam(':title', $this->title);
		$stmt->bindParam(':body', $this->body);
		$stmt->bindParam(':author', $this->author);
		$stmt->bindParam(':category_id', $this->category_id);

		# Execute Query
		$result = $stmt->execute();

		if ($result)
		{
			return true;
		}

		printf("ERROR: %s. \n", $stmt->error);

		return false;
	}

	# Update Post (UPDATE METHOD)
	public function updatePost() : bool
	{
		$query = 'UPDATE '
					. $this->table . '
				SET
					title = :title,
					body = :body,
					author = :author,
					category_id = :category_id
				WHERE
					id = :id
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Clean Data
		$this->title = htmlspecialchars(strip_tags($this->title));
		$this->body = htmlspecialchars(strip_tags($this->body));
		$this->author = htmlspecialchars(strip_tags($this->author));
		$this->category_id = htmlspecialchars(strip_tags($this->category_id));

		$this->id = htmlspecialchars(strip_tags($this->id));

		# Bind data
		$stmt->bindParam(':title', $this->title);
		$stmt->bindParam(':body', $this->body);
		$stmt->bindParam(':author', $this->author);
		$stmt->bindParam(':category_id', $this->category_id);

		$stmt->bindParam(':id', $this->id);

		# Execute Query
		$result = $stmt->execute();

		if ($result)
		{
			return true;
		}

		printf("ERROR: %s. \n", $stmt->error);

		return false;
	}

	# Delete Post (DELETE METHOD)
	public function deletePost() : bool
	{
		$query = 'DELETE FROM '
					. $this->table . '
				WHERE
					id = :id
				';

		# Prepare Statement
		$stmt = $this->conn->prepare($query);

		# Clean Data
		$this->id = htmlspecialchars(strip_tags($this->id));

		# Bind ID
		$stmt->bindParam(':id', $this->id);

		# Execute Query
		$exec = $stmt->execute();

		if ($exec)
		{
			return true;
		}

		printf("ERROR: %s. \n", $stmt->error);

		return false;
	}
}

?>