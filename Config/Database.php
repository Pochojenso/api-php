<?php
	
/**
 * Class Connection
 */
class Database
{
	# Database properties
	private $host = 'YOUR_HOSTNAME';
	private $db_name = 'YOUR_DATABASE_NAME';
	private $username = 'YOUR_USERNAME';
	private $password = 'YOUR_PASSWORD';
	private $conn;

	public function connect_to_DB() : object
	{
		$this->conn = null;

		try
		{
			$this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' .  $this->db_name,
									$this->username, $this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		}
		catch (PDOException $e)
		{
			echo 'CONNECTION ERROR: ' . $e->getMessage();
		}

		return $this->conn;
	}
}

?>