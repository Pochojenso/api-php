<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once('../../Config/Database.php');
	include_once('../../Models/Category.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Category Class (NEW OBJECT)
	$category = new Category($db);

	# GET ID (CATCH VALUE Sent From Client)
	if (isset($_GET['id']) && !empty($_GET['id'])) {
		$category->id = $_GET['id'];
	}
	else
	{
		print_r(json_encode( array('message' => 'ID Not Especified') ));
		die();
	}

	# GET Category
	$category->getCategory();

	# Create Array to return as JSON
	$category_arr = array(
		'id' => $category->id,
		'name' => $category->category_name
	);

	# Build JSON to return
	( empty($category_arr['name']) ) ? print_r(json_encode( array('message' => 'Category Not Found'))) : print_r(json_encode($category_arr));
?>

