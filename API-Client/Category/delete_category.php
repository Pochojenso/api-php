<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: DELETE');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-with');

	include_once('../../Config/Database.php');
	include_once('../../Models/Category.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Category Class (NEW OBJECT)
	$category = new Category($db);

	# GET Raw posted ID
	$data = json_decode(file_get_contents('php://input'));

	# SET ID to Delete
	$category->id = $data->id;

	# Delete Category
	$execute_method = $category->deleteCategory();

	if ($execute_method)
	{
		echo json_encode(
			array('message' => 'Category Deleted')
		);
	}
	else
	{
		echo json_encode(
			array('message' => 'Category Not Deleted')
		);
	}

?>