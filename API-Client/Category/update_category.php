<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: PUT');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-with');

	include_once('../../Config/Database.php');
	include_once('../../Models/Category.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Category Class (NEW OBJECT)
	$category = new Category($db);

	# GET raw posted data
	$data = json_decode(file_get_contents('php://input'));

	# SET ID to Update
	$category->id = $data->id;

	$category->name = $data->name;

	# Update Category
	$execute_method = $category->updateCategory();

	if ($execute_method)
	{
		echo json_encode(
			array('message' => 'Category Updated')
		);
	}
	else
	{
		echo json_encode(
			array('message' => 'Category Not Updated')
		);
	}

?>