<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once('../../Config/Database.php');
	include_once('../../Models/Category.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Category Class (NEW OBJECT)
	$category = new Category($db);

	#Blog Category
	$result = $category->getCategories();

	# Get Row Count
	$rows = $result->rowCount();

	# Check if exists Categories
	if ($rows > 0)
	{
		# Category Array
		$category_arr = array();
		$category_arr['data'] = array();

		while ($row = $result->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);

			$category_item = array(
				'id' => $id,
				'name' => $name
			);

			# Push to 'data' with array_push() function php
			array_push($category_arr['data'], $category_item);

		}

		# Build - Turn to JSON and OUTPUT
		echo json_encode($category_arr);

	}
	else
	{
		# No Categories found
		echo json_encode(
			array('message' => 'Categories Not Found')
		);
	}
	

?>