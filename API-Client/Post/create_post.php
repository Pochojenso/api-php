<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-with');

	include_once('../../Config/Database.php');
	include_once('../../Models/Post.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Post Class (NEW OBJECT)
	$post = new Post($db);

	# GET raw posted data
	$data = json_decode(file_get_contents('php://input'));

	$post->title = $data->title;
	$post->body = $data->body;
	$post->author = $data->author;
	$post->category_id = $data->category_id;

	# Create Post
	$execute_method = $post->createPost();

	if ($execute_method)
	{
		echo json_encode(
			array('message' => 'Post Created')
		);
	}
	else
	{
		echo json_encode(
			array('message' => 'Post Not Created')
		);
	}

?>