<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once('../../Config/Database.php');
	include_once('../../Models/Post.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Post Class (NEW OBJECT)
	$post = new Post($db);

	#Blog Post
	$result = $post->getPosts();

	# Get Row Count
	$rows = $result->rowCount();

	# Check if exists posts
	if ($rows > 0)
	{
		# Post Array
		$post_arr = array();
		$post_arr['data'] = array();

		while ($row = $result->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);

			$post_item = array(
				'id' => $id,
				'title' => $title,
				'body' => html_entity_decode($body),
				'author' => $author,
				'category_id' => $category_id,
				'category_name' => $category_name
			);

			# Push to 'data' with array_push() function php
			array_push($post_arr['data'], $post_item);

		}

		# Build - Turn to JSON and OUTPUT
		echo json_encode($post_arr);

	}
	else
	{
		# No posts found
		echo json_encode(
			array('message' => 'Posts Not Found')
		);
	}
	

?>