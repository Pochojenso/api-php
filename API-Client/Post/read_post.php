<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once('../../Config/Database.php');
	include_once('../../Models/Post.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Post Class (NEW OBJECT)
	$post = new Post($db);

	# GET ID (CATCH VALUE Sent From Client)
	if (isset($_GET['id']) && !empty($_GET['id'])) {
		$post->id = $_GET['id'];
	}
	else
	{
		print_r(json_encode( array('message' => 'ID Not Especified') ));
		die();
	}

	# GET POST
	$post->getPost();

	# Create Array to return as JSON
	$post_arr = array(
		'id' => $post->id,
		'title' => $post->title,
		'body' => $post->body,
		'author' => $post->author,
		'category_id' => $post->category_id,
		'category_name' => $post->category_name
	);

	# Build JSON to return
	( empty($post_arr['body']) ) ? print_r(json_encode( array('message' => 'Post Not Found'))) : print_r(json_encode($post_arr));
?>

