<?php
	# Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: DELETE');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-with');

	include_once('../../Config/Database.php');
	include_once('../../Models/Post.php');

	# Instantiate Database Class and Connect (NEW OBJECT)
	$database = new Database();
	$db = $database->connect_to_DB();

	# Instantiate Post Class (NEW OBJECT)
	$post = new Post($db);

	# GET Raw Posted ID
	$data = json_decode(file_get_contents('php://input'));

	# SET ID to Delete
	$post->id = $data->id;

	# Delete Post
	$execute_method = $post->deletePost();

	if ($execute_method)
	{
		echo json_encode(
			array('message' => 'Post Deleted')
		);
	}
	else
	{
		echo json_encode(
			array('message' => 'Post Not Deleted')
		);
	}

?>